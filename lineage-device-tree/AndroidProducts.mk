#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_PD2188.mk

COMMON_LUNCH_CHOICES := \
    lineage_PD2188-user \
    lineage_PD2188-userdebug \
    lineage_PD2188-eng
